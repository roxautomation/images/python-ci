#!/bin/bash

# this script will get lint an external repo.

source ./config.sh
./build.sh

# clean old repo if it exists
rm -rf /tmp/roxbot

# clone repo
git clone https://github.com/rox-automation/roxbot.git /tmp/roxbot


# lint repo
docker run --rm -v /tmp/roxbot:/workspace $IMG_NAME



