#!/bin/bash

# build multiarch image, proviede --push to push to dockerhub


set -e
source ./config.sh
source ~/.secrets

docker login -u $DOCKERHUB_USER -p $DOCKERHUB_TOKEN

# create builder, continue if already exists
docker buildx create --name mybuilder --use --bootstrap || true


docker buildx build --platform linux/amd64,linux/arm64 -t $IMG_NAME $1 docker

# remove builder
#docker buildx rm mybuilder
