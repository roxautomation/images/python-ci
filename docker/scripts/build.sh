#!/bin/bash
WORKDIR="/workspace"

echo "building the code in $WORKDIR"

set -e
set -x

# lint first
/scripts/lint.sh

# build the package
cd $WORKDIR
rm -rf dist
python -m build

