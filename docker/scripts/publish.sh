#!/bin/bash

# publish python package to pypi

set -e
set -x

# check if PYPI_TOKEN is set
if [ -z "$PYPI_TOKEN" ]; then
    echo "PYPI_TOKEN not set"
    exit 1
fi

# build the package
cd /workspace
rm -rf dist
python -m build

# publish the package
twine upload -u __token__ -p $PYPI_TOKEN dist/*
