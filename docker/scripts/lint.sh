#!/bin/bash
WORKDIR="/workspace"
echo "Image version: "
cat /build_date.txt

echo "Linting the code in $WORKDIR"

set -e
set -x

python --version

cd $WORKDIR
ls -la
pip install  -e .[ci]

pylint -E src
mypy src
pytest --cov=src --cov-report term-missing tests


