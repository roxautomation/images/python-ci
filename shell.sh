#!/bin/bash

set -e

source ./config.sh
./build.sh
docker run -it --rm $IMG_NAME bash
