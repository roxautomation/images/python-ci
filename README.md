# python-ci docker image


This image is used for running CI scripts locally.

The reason for this is independancy from cloud platforms. Gitlab ci and github actions are nice, but require quite some management. Managing multiple repositories on two platform becama a major burden. This repo is an attempt to streamline the process.


## How it works

* mount repo on `/workspace`
* a ci script will be run from `/scripts` directory.


## Examples

**lint**

    docker run --rm -v $(pwd):/workspace roxauto/python-ci

**publish to pypi**

    docker run --rm -e PYPI_TOKEN=$PYPI_TOKEN -v $(pwd):/workspace roxauto/python-ci /scripts/publish.sh




## Optional requirements

Add optional ci dependencies to `pyproject.toml` like this:

    [tool.setuptools.dynamic]
    dependencies = {file = ["requirements.txt"]}
    optional-dependencies = {ci = { file = ["requirements-ci.txt"] }}

**Note:** using a separate requirements file is more flexible than including pakages directly.


## Using invoke

example invoke task:

```python
@task
def ci(ctx):
    """
    run ci locally in a fresh container

    """
    t_start = time.time()
    # get script directory
    script_dir = os.path.dirname(os.path.realpath(__file__))
    try:
        ctx.run(f"docker run --rm -v {script_dir}:/workspace roxauto/python-ci")
    finally:
        t_end = time.time()
        print(f"CI run took {t_end - t_start:.1f} seconds")


@task(pre=[ci])
def publish(ctx):
    """publish package to pypi"""
    script_dir = os.path.dirname(os.path.realpath(__file__))

    token = os.getenv("PYPI_TOKEN")
    if not token:
        raise ValueError("PYPI_TOKEN environment variable is not set")

    ctx.run(
        f"docker run --rm -e PYPI_TOKEN={token} -v {script_dir}:/workspace roxauto/python-ci /scripts/publish.sh"
    )

```
